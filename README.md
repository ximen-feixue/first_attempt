# first_attempt

#### 介绍
一群志同道合的小伙伴，做感兴趣的事，目标上岸大厂

#### 软件架构

​	TODO

#### 参与开发

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

### 组织结构

``` lua
attempt
├── attempt-adapter -- 适配第三方服务
├── attempt-auth -- 权限相关
├── attempt-admin -- 后台管理系统接口
├── attempt-atomic -- 原子层-数据库交互
├── attempt-common -- 工具类及通用代码
├── attempt-config -- 公共配置
├── attempt-core -- 核心设计
├── attempt-mbg -- MyBatisGenerator生成的数据库操作代码
├── attempt-security -- 封装公用安全模块
├── attempt-search -- 搜索服务
├── attempt-portal -- 网关接口层
├── attempt-service -- RPC接口层
└── attempt-demo -- 测试代码
```

### 技术选型参考

| 技术                 | 版本  | 说明                | 官网                                           |
| -------------------- | ----- | ------------------- | ---------------------------------------------- |
| SpringBoot           | 2.0.0 | 容器+MVC框架        | https://spring.io/projects/spring-boot         |
| SpringSecurity       |       | 认证和授权框架      | https://spring.io/projects/spring-security     |
| MyBatis              |       | ORM框架             | http://www.mybatis.org/mybatis-3/zh/index.html |
| MyBatisGenerator     |       | 数据层代码生成      | http://www.mybatis.org/generator/index.html    |
| Elasticsearch        |       | 搜索引擎            | https://github.com/elastic/elasticsearch       |
| RabbitMQ             |       | 消息队列            | https://www.rabbitmq.com/                      |
| Redis                |       | 分布式缓存          | https://redis.io/                              |
| MongoDB              |       | NoSql数据库         | https://www.mongodb.com                        |
| LogStash             |       | 日志收集工具        | https://github.com/elastic/logstash            |
| Kibana               |       | 日志可视化查看工具  | https://github.com/elastic/kibana              |
| Nginx                |       | 静态资源服务器      | https://www.nginx.com/                         |
| Docker               |       | 应用容器引擎        | https://www.docker.com                         |
| Jenkins              |       | 自动化部署工具      | https://github.com/jenkinsci/jenkins           |
| Druid                |       | 数据库连接池        | https://github.com/alibaba/druid               |
| OSS                  |       | 对象存储            | https://github.com/aliyun/aliyun-oss-java-sdk  |
| MinIO                |       | 对象存储            | https://github.com/minio/minio                 |
| JWT                  |       | JWT登录支持         | https://github.com/jwtk/jjwt                   |
| Lombok               |       | 简化对象封装工具    | https://github.com/rzwitserloot/lombok         |
| Hutool               |       | Java工具类库        | https://github.com/looly/hutool                |
| PageHelper           |       | MyBatis物理分页插件 | http://git.oschina.net/free/Mybatis_PageHelper |
| Swagger-UI           |       | 文档生成工具        | https://github.com/swagger-api/swagger-ui      |
| Hibernator-Validator |       | 验证框架            | http://hibernate.org/validator                 |
| Mysql                | 5.7   | 关系型数据库        | https://dev.mysql.com/                         |

#### 架构图

##### 系统架构图

TODO

##### 业务架构图

TODO

#### 开发进度
TODO 持续更新。。

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
