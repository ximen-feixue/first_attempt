package com.attempt.first.adapter;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AttemptAdapterApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void testLog() throws Exception {
	}

}
